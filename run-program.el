;; -*- lexical-binding: t -*-

;;;; run-program
(defvar run-program-history nil
  "History of programs started using `run-program'.")

(defvar run-program-programs-list nil
  "List of program names with commands to run them in form of
  Dotted Pair notation `(name . command)'.")

(defun run-program-onfinished (process event)
  "Handle on `finished\n' EVENT by killing PROCESS buffer."
  (when (string= "finished\n" event)
    (kill-buffer (process-buffer process))
    (message "`%s' process has finished" (process-name process))))

(defun run-program (name command)
  "Start process NAME with given COMMAND.  When process is
finished buffer will be removed.  With prefix argument you will
be asked to provide custom COMMAND."
  (interactive
   (if current-prefix-arg
       (list nil (read-from-minibuffer "Custom command: "))
     (let* ((name (completing-read "Run: " run-program-programs-list nil t nil 'run-program-history))
            (command (cdr (seq-find (lambda (x) (equal (car x) name)) run-program-programs-list))))
       (list name command))))
  (let* ((index-of-space (string-match " " command))
         process-args buffer-name program program-args)
    ;; TODO support multiple process program arguments
    (if (not index-of-space) (setq program command)
      (setq program (substring command 0 index-of-space))
      (setq program-args (substring command (+ 1 index-of-space) (length command))))
    (if (not name) (setq name program))
    (setq buffer-name (format " *%s*" name))
    (setq process-args (list name buffer-name program))
    (if program-args (add-to-list 'process-args program-args t))
    (message "Starting process `%s' in buffer `%s'" name buffer-name)
    (set-process-sentinel (apply 'start-process process-args)
                          'run-program-onfinished)))

(setq run-program-programs-list
      '(("Firefox"               . "firefox")
        ("Chrome"                . "google-chrome")
        ("Discord"               . "discord")
        ("Krita 4.2.7.1b"        . "~/krita-4.2.7.1b-x86_64.appimage")
        ("LBRY 0.43.4"           . "~/LBRY_0.43.4.AppImage")
        ("Blender 2.80"          . "~/blender-2.80-linux-glibc217-x86_64/blender")
        ("Blender 2.79b"         . "~/Projekty/gothic/blender-2.79b-linux-glibc219-x86_64/blender")
        ("Blender 2.66a (wine)"  . "wine /home/irek/Projekty/gothic/blender-2.66a-windows32/blender")
        ("Blender 2.57b (wine)"  . "wine /home/irek/Projekty/gothic/blender-2.57b-windows32/blender")
        ("Gothic Sourcer (wine)" . "wine /home/irek/.wine/drive_c/Program\\ Files\\ \\(x86\\)/GothicSourcer\\ V3.14/GothicSourcer/GothicSourcerV3_14.exe")))

(global-set-key (kbd "C-c p") 'run-program)
